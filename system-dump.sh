#!/usr/bin/env bash

sudo dpkg-query -f '${binary:Package}\n' -W > debian-packages.list
nix-env -q > nix-installed.list
flatpak list --columns=application,version,branch > flatpack-installed.list
snap list | grep -v disabled > snap-installed.list
brew list > brew-installed.list

