#!/usr/bin/env bash

set -e
set pipefail

mkdir -p $HOME/.config

install_linux_deps() {
  if [ -f /etc/redhat-release ] ; then
      DistroBasedOn='RedHat'
      DIST=`cat /etc/redhat-release |sed s/\ release.*//`
      PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
      REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
  elif [ -f /etc/SuSE-release ] ; then
      DistroBasedOn='SuSe'
      PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
      REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
  elif [ -f /etc/mandrake-release ] ; then
      DistroBasedOn='Mandrake'
      PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
      REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
  elif [ -f /etc/debian_version ] ; then
      DistroBasedOn='Debian'
      DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
      PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
      REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
  fi

  # Install nix
  if ! command -v nix-env &> /dev/null; then
    sh <(curl -L https://nixos.org/nix/install) --daemon
  fi

  # Install linuxbrew
  if ! command -v brew &> /dev/null; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  fi


  case $DistroBasedOn in
    Debian)
      sudo apt-get update
      sudo apt-get install flatpak git build-essential git-man \
                           apt-transport-https ca-certificates gnupg \
                           docker.io docker-compose make

      echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

      if command -v apt-key &> /dev/null; then
        curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
      else
        curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo tee /usr/share/keyrings/cloud.google.gpg
      fi

      # wget 'https://github.com/neovim/neovim/releases/download/v0.6.1/nvim.appimage' -O ./Downloads/nvim.0.6.1.appimage

      flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

      sudo apt-get update

      install_1password_deb
      sudo dpkq -i ~/Downloads/Insomnia.Core-2022.1.0.deb
      sudo dpkq -i ~/Downloads/zoom_amd64.deb
      sudo dpkq -i ~/Downloads/code_1.62.2-1636665017_amd64.deb

      sudo xargs -a ./debian-packages.list apt install
      ;;
    *)
      echo "Don't know how to install packages on this system"
      ;;
  esac

  cat ./flatpack-installed.list | cut -f 1 | xargs -n 1 flatpak install
  xargs -n 1 -a ./nix-installed.list nix-env -i
  xargs -n 1 -a ./brew-installed.list brew install
}

install_1password_deb() {
  curl -sS https://downloads.1password.com/linux/keys/1password.asc \
    | sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
  echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' \
    | sudo tee /etc/apt/sources.list.d/1password.list
  sudo mkdir -p /etc/debsig/policies/AC2D62742012EA22/
  curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
  sudo mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
  curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
  sudo apt update
  sudo apt install 1password
}

configure_system() {
  echo "Configuring docker"
  sudo usermod -aG docker $USER
}

install_macos_deps() {
  # install brew
  # install list of brew packages
}

case $OSTYPE in
  linux*)
    install_linux_deps
    ;;
  darwin*)
    install_macos_deps
    ;;
  *)
    echo "Unknown operating system" ;;
esac

configure_system

# Install bash config

# Install nvim config

# Install kitty config

if [ -d "$HOME/.asdf" ]; then
  (
   cd "$HOME/.asdf"
   git pull origin master
  )
else
  git clone https://github.com/asdf-vm/asdf.git "$HOME/.asdf"
fi

asdf_plugin_update() {
  if ! asdf plugin-list | grep -Fq "$1"; then
    asdf plugin-add "$1"
  fi

  asdf plugin-update "$1"
}

asdf_plugin_update "golang"
asdf_plugin_update "java"
asdf_plugin_update "clojure"
asdf_plugin_update "nodejs"
asdf_plugin_update "python"
asdf_plugin_update "ruby"

export NODEJS_CHECK_SIGNATURES=no

# Installs all versions listed in .tool-versions
asdf install
